/* ---------------------------------------------------------------
			RE-INITIALISATION DES TAGS HTML
	Pour éviter les valeurs par défaut différentes suivant les 
	navigateurs. Seuls les tags le plus	souvent utilisés et non 
	dépréciés sont pris en compte.
	Reset complet : http://meyerweb.com/eric/tools/css/reset/
--------------------------------------------------------------- */
html, body, div, span, applet, iframe,
h1, h2, h3, h4, h5, h6, p, pre,
a, img, strong, dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td {
	margin: 0;
	padding: 0;
	border: 0;
	outline: 0;
	font-size: 100%;
	font-weight: normal;	
	vertical-align: baseline;
	background: transparent;
	}
body {
	line-height: 1;
	}	
ol, ul {
	list-style: none;
	}
table { 
	border-collapse: collapse;
	border-spacing: 0;
	}
	
/* ---------------------------------------------------------------
	FEUILLE DE STYLE ORIGINALE	
--------------------------------------------------------------- */
html {
	height: 100%
	}
	
body {
	font-family: Verdana, Arial, Sans-Serif;
	font-size: 13px;  /*10pt;	*/
  	color: #404040;
	background: #9ac5e7 url(../images/fond.jpg);  	
	}
	
p {
	padding: 10px 5px;	
}

a {
	color: #3c96f1;
	text-decoration: none;
}
	
a:hover {
	text-decoration: underline;
}
	
strong {
	font-weight: bold;
}


#bcPage {
	margin: 10px auto;
	width: 1030px;
	position: relative;
}


	
/* ---------------------------------------------------------------
	BLOC ENTETE
--------------------------------------------------------------- */
#bcEntete {
	background: url(../images/tete.png) no-repeat; 
	height: 135px;
	position: relative;
}

#bcLogo {
	background: url(../images/logo.png) no-repeat;
	width: 285px;
	height: 45px;
	position: absolute;
	top: 10px;
	left: 30px;
}

	
#btnDeconnexion {
	background: url(../images/deconnexion.png) no-repeat;
	height: 36px;
	width: 22px;
	position: absolute;
	right: 30px;
	top: 10px;
}
	
	
#bcOnglets { 
	position: absolute;
	left: 321px;
	top: 5px;
}
	
#bcOnglets a,
#bcOnglets h2 {
	text-decoration: none; 
    text-align: center;
	font: bold 16px Verdana;    
	color: #757575; 
	background: url(../images/onglet_off.png) no-repeat; 
	float: left;
	width: 158px;
	height: 24px;
	margin-right: 1px;
	padding-top: 7px;
}

#bcOnglets a:hover { 
	color: #ff9b37; 
}

#bcOnglets h2 {
	background: url(../images/onglet_on.png) no-repeat; 
	color: #ff9b37;
	height: 27px;
}
	

	
/* ---------------------------------------------------------------
	BLOC PIED DE PAGE
--------------------------------------------------------------- */
#bcPied {
	position: relative;
	clear: both;
	height: 156px;
	background: transparent url(../images/pied.png) no-repeat;		
	}

#bcPied a {
	text-decoration: none; 
	font: 14px Verdana;    
	color: #404040; 
	position: absolute;
	background: transparent url(../images/fleche_pied.png) no-repeat;
	padding-left: 22px;
	height: 22px;
	}
	
#apropos {	
	left: 198px;
	top: 55px;
	}		

#confident {	
	left: 360px;
	top: 75px;
	}
		
#conditions {	
	left: 598px;
	top: 95px;
	}
	
#bcPied a:hover {
	text-decoration: underline;
	color: #000; 
}
	
#copyright {
	font: 14px Verdana;    
	position: absolute;	
	left: 792px;
	top: 55px;
}


/* ---------------------------------------------------------------
	BLOCS DE CONTENU
--------------------------------------------------------------- */

#bcContenu {
	position: relative;
	background: url(../images/contenu.png) repeat-y;
	min-height: 500px;
	padding: 0px 10px 20px 10px;
}

#bcGauche {
	float: left;	
	width: 250px;
	margin: 10px 10px;
}

#bcCentre {
	position: relative;
	display: block;
	margin-left: 250px;
}

/* ---------------------------------------------------------------
	BLOC CALENDRIER
--------------------------------------------------------------- */

#calendrier {
	background-color: #FFFFFF;
	border: solid 2px #9ac5e7;
	width: 235px;
	text-align: center;
	padding: 1px;
	margin-bottom: 20px;
}	

#calendrier table {
	width: auto;
	margin: 10px auto;	
}

#calendrier p {
	background-color: #FFF;
	text-align: center;
	margin-bottom: 5px;
	padding: 10px;		
}

#calendrier td a {
	display: block;
	width: 30px;
	height: 22px; 	
	line-height: 13px;
	padding-top: 7px;
	color: #000000;
}

#calendrier a:hover {
	text-decoration: none;	
}

#calendrier a.lienJourHorsMois {
	color: #999999;	
}

#calendrier th {
	font-weight: bold;
	height: 20px;
}

#calendrier td {
	border: solid 1px #000000;
	color: #000000;	
} 

.flechegauche {
	float: left;
	width: 20px;
	height: 20px;	
}

.flechedroite {
	float: right;
	width: 20px;
	height: 20px;	
}

.aligncenter {
	text-align:center;	
}

.jourCourant {
	background-color: #5555FF;	
}

.semaineCourante {
	background-color: #9ac5e7;
}

.aujourdHui {
	background-color: #FF5555;
}


/* ---------------------------------------------------------------
	BLOC AFFICHAGE DES CATEGORIES
--------------------------------------------------------------- */

#categories {
	background-color: #FFFFFF;
	border: solid 2px #9ac5e7;
	width: 235px;
	padding: 1px 1px 10px 1px;
}


#categories h3 {
	background-color: #9ac5e7;
	padding: 10px;	
	font-weight: bold;
	font-size: 13px; 
	text-align: center;
}

#categories p {
	padding: 10px 10px 5px 10px;
	color: #007acc;	
}

#categories ul {
	margin-bottom: 10px;
}
	
#categories li {
	padding-top: 2px; 
}	

.categorie {
	float: left; 
	margin: 0px 5px 0px 20px;
	width: 10px; 
	height: 10px; 
}

.groupeA {
	border: solid 2px #00DD00; 
	background-color: #00FF00;
}
.groupeB {
	border: solid 2px #DD0000; 
	background-color: #FF0000;
}
.groupeC {
	border: solid 2px #0000DD; 
	background-color: #0000FF;
}

/* ---------------------------------------------------------------
	RENDEZ-VOUS ET EVENEMENTS
--------------------------------------------------------------- */

.rendezvous {
	display: block;
	color: #000;
	font-size: 11px; 
	position: absolute; 
    margin: 1px; 
    padding: 2px 0px 0px 2px; 
    color: #000000;
    width: 87px; 							          
}


/* ---------------------------------------------------------------
	AGENDA
--------------------------------------------------------------- */
#titreAgenda {
	text-align: center;
	width: 672px; 
	margin: 0 auto; 
	padding-left: 30px;
}

#agenda { 
 	position: relative;
 	margin: 0 auto 0px auto;
 	width: 746px;
 	height: 540px;
 } 
 
#agenda a { 
 	color: #FFFFFF; 
 	text-decoration: none;
	display: block;
	height: 40px; 
	background-color: transparent;
	border-bottom: dashed 1px #5555FF;	
}

#agenda a.case-heure-bas {
	border-bottom: none;
}

#agenda a:hover { 
	color: #FFFFFF; 
	text-decoration: underline;
	background-color: #9ac5e7;
}

#intersection { 
	float: left; 
	height: 15px; 
	width: 30px; 
}

#col-heures { 
	float: left;
	width: 30px;
}

#col-heures div {
	height: 41px; 
	font-size: 9px; 
	right: 0px; 
	padding-right: 1px;
	text-align: right; 
}

.border-TRB {	
	border-top: solid 2px #5555FF; 
	border-right: solid 2px #5555FF; 
	border-bottom: solid 2px #5555FF; 
}
 
.border-L { 
	border-left: solid 2px #5555FF; 
}

.col-jour { 
	margin-top: 1px; 
	float: left; 
	width: 95px; 
}

.case-jour { 
	background-color: #5555FF;
	float: left; 
	width: 95px; 
	height: 20px;
	line-height: 20px;
	text-align: center; 
	color: #FFF;
}

/* ---------------------------------------------------------------
	FORMULAIRE ET ERREURS
--------------------------------------------------------------- */
.center-page {
	text-align: center;
	line-height: 2em;
}

form {
	display: inline-block;
	padding-top: 10px;
}

form td {
	padding-right: 0.8em;
}

form td:nth-of-type(odd) {
	text-align: right;
}

form td:nth-of-type(even) {
	text-align: left;
}

legend {
	font-weight: bold;
	border-bottom: 2px solid black;
	width: 100%;
	text-align: left;
	float: left;
	margin-bottom: 1em;
}

#errors {
	margin: 0;
}

#redirect {
	float: left;
}

/* ---------------------------------------------------------------
	BOUTONS
--------------------------------------------------------------- */

.btnValider, .btnAnnuler, .btnSearch {
	font-weight: bold;
	cursor: pointer;
	border-radius: 0.5em;
	width: 100px;
	color: white;
	background-color: #ff8533;
	padding: 4px 0;
	margin: 10px 0;
}

.btnAbonne {
	float: right;
	font-weight: bold;
	cursor: pointer;
	border-radius: 0.5em;
	width: 120px;
	color: white;
	background-color: #ff8533;
	margin: 5px 1em;
	padding: 4px 0;
}

form tr:last-of-type td:last-of-type {
	text-align: left;
}

/* ---------------------------------------------------------------
	LISTES ABONNEMENTS + RECHERCHE
--------------------------------------------------------------- */

li {
	list-style: none;
}

#utiSearch {
	display: inline-block;
	width: 90%;
	text-align: left;
	position: absolute;
	top: 70px;
	right: 65px;
}

#utiSearch li, #utiFollows li, #utiFollowings li {
	display: inline-block;
	width: 100%;
	height: 3em;
	padding-left: 1em;
	line-height: 3em;
}

#utiSearch li:nth-child(odd), #utiFollows li:nth-child(odd), #utiFollowings li:nth-child(odd) {
	background-color: #9AC5E7;
}

#utiSearch li:nth-child(even), #utiFollows li:nth-child(even), #utiFollowings li:nth-child(even) {
	background-color: #E5ECF6;
}

#utiFollows {
	margin: 1em 0 1em 0;
	display: inline-block;
	width: 95%;
}

#utiFollowings {
	margin-top: 1em;
	display: inline-block;
	width: 95%;
}

#utiFollows h3, #utiFollowings h3 {
	margin-bottom: 10px;
	font-weight: bold;
}

/* ---------------------------------------------------------------
	PARAMETRES
--------------------------------------------------------------- */
#paramPage .warning {
	border: 2px solid orange;
	color: orange;
	font-weight: bold;
	text-align: center;
	width: 80%;
	display: block;
	margin: 0 auto;
	clear: both;
	padding: 0.5em 0;
}

#paramPage {
	line-height: 2em;
	margin: 0 7em;
}

#paramPage table {
	margin: 0 auto;
}

#paramPage form{
	width: 828px;
	text-align: center;
}

#paramPage #newCat {
	border: none;
	margin: 0.5em 0 -0.5em 94px;
}

#paramPage label {
	margin: 4px 3px 3px 4px;
}

#paramPage input[type=image] {
	margin: 0 0 -4px 8px;
}
