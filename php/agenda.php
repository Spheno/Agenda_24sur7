<?php
/** @file
 * Page d'accueil de l'application 24sur7
 *
 * @author : Frederic Dadeau - frederic.dadeau@univ-fcomte.fr
 */

include('bibli_24sur7.php');	// Inclusion de la bibliothéque

session_start();

jl_verifie_session();

// Connexion à la bdd
jl_bd_connexion();

jl_html_head('24sur7 | Agenda');

jl_html_bandeau(APP_PAGE_AGENDA);

echo '<section id="bcContenu">',
		'<aside id="bcGauche">';

/*
Agenda à afficher :
  - Soit en GET (vérifications à effectuer)
  - Soit celui de la personne dans $_SESSION
*/

$nomAgenda = $_SESSION['utiNom'];
$idAgenda = $_SESSION['utiID'];
$owner = true;

if(isset($_GET['agenda'])) {
  if(is_numeric($_GET['agenda']) && $_GET['agenda'] != $idAgenda) {
    // Vérification de la validité de l'id
    $sql = 'SELECT utiNom
            FROM utilisateur
            WHERE utiID=' . $_GET['agenda'];
    $R = mysqli_query($GLOBALS['bd'], $sql) or jl_bd_erreur($sql);
    if($D = mysqli_fetch_assoc($R)) {
      $owner = false;
      $nomAgenda = $D['utiNom'];
      $idAgenda = $_GET['agenda'];
    }
    mysqli_free_result($R);
  }
}

/*
  Requete pour savoir ce qu'il faut afficher (jours, heureMin et heureMax)
*/
$sql = "SELECT utiJours, utiHeureMin, utiHeureMax
        FROM utilisateur
        WHERE utiID={$idAgenda}";
$R = mysqli_query($GLOBALS['bd'], $sql) or jl_bd_erreur($sql);
if($res = mysqli_fetch_assoc($R)) {
  $joursAffiches = $res['utiJours'];
  $heureMinAffiche = $res['utiHeureMin'];
  $heureMaxAffiche = $res['utiHeureMax'];
} else {
  $joursAffiches = 127;
  $heureMinAffiche = 8;
  $heureMaxAffiche = 20;
}
mysqli_free_result($R);

/*
Récupération de la semaine à afficher :
 - Soit les données sont en GET, et on affiche,
 - Soit on utilise la date actuelle
*/

// Date actuelle
list($JJ, $MM, $AA) = explode('-', date('j-n-Y'));

$jour = isset($_GET['jour']) ? $_GET['jour'] : $JJ;
$mois = isset($_GET['mois']) ? $_GET['mois'] : $MM;
$annee = isset($_GET['annee']) ? $_GET['annee'] : $AA;

if (!checkdate($mois, $jour, $annee)) {
  $jour = $JJ;
  $mois = $MM;
  $annee = $AA;
}

jl_html_calendrier($jour, $mois, $annee, $owner ? '' : 'agenda=' . $idAgenda);
$jourSemaine = date('N', mktime(0, 0, 0, $mois, $jour, $annee)) - 1; // 0-6

$dateDebutSemaineCourante = jll_gen_date($annee, $mois, $jour, -$jourSemaine);

$dateFinSemaineCourante = jll_gen_date($annee, $mois, $jour, 6 - $jourSemaine);
// $jll_date_jour($dateDebutSemaineCourante) = $jour - $jourSemaine;
// $jll_date_jour($dateFinSemaineCourante) = $jour + 6 - $jourSemaine;

echo '<section id="categories">';

// Catégories

jl_html_categories();


echo '</section>',
		'</aside>';

// Semainier
$dateSemainePrec = jll_gen_date(jll_date_annee($dateDebutSemaineCourante), jll_date_mois($dateDebutSemaineCourante), jll_date_jour($dateDebutSemaineCourante), -7);

echo '<section id="bcCentre">',
        '<p id="titreAgenda">',
        '<a href="',
        '?annee=', jll_date_annee($dateSemainePrec),
        '&mois=', jll_date_mois($dateSemainePrec),
        '&jour=', jll_date_jour($dateSemainePrec);
if(!$owner) {
  echo '&agenda=', $idAgenda;
}
echo '" class="flechegauche"><img src="../images/fleche_gauche.png" alt="picto fleche gauche"></a>';

echo '<strong>Semaine du ', jll_date_jour($dateDebutSemaineCourante),' au ', jll_date_jour($dateFinSemaineCourante) ,' ', jl_get_mois($mois) ,'</strong> pour <strong>', $nomAgenda ,'</strong>';

$dateSemaineSuiv = jll_gen_date(jll_date_annee($dateDebutSemaineCourante), jll_date_mois($dateDebutSemaineCourante), jll_date_jour($dateDebutSemaineCourante), 7);

echo  '<a href="',
      '?annee=', jll_date_annee($dateSemaineSuiv),
      '&mois=', jll_date_mois($dateSemaineSuiv),
      '&jour=', jll_date_jour($dateSemaineSuiv);
if(!$owner) {
  echo '&agenda=', $idAgenda;
}
echo '" class="flechedroite"><img src="../images/fleche_droite.png" alt="picto fleche droite"></a>',
      '</p>',
      '<section id="agenda">',
      '<div id="intersection"></div>';

for($i = 1; $i <= 7; $i++) {
  if($joursAffiches < pow(2, 7 - $i) || substr(decbin($joursAffiches), -1 - (7-$i), 1) === '0') {
    continue;
  }
  $border = 'border-TRB';
  if($i == 0)
    $border .= ' border-L';
  echo '<div class="case-jour ', $border ,'">', jl_get_jour($i), ' ', jll_date_jour(jll_gen_date(jll_date_annee($dateDebutSemaineCourante), jll_date_mois($dateDebutSemaineCourante), jll_date_jour($dateDebutSemaineCourante), $i - 1)), '</div>';
}
echo '<br><br>';
echo '<div id="col-heures">';

for($i = $heureMinAffiche; $i <= $heureMaxAffiche; $i++) {
  echo '<div>', $i, 'h</div>';
}

echo '</div>';

$first = true;
for($i = 0; $i < 7; $i++) {
  if($joursAffiches < pow(2, 6 - $i) || substr(decbin($joursAffiches), -1 - (6-$i), 1) === '0') {
    continue;
  }
  // Date de chaque journée : date complète pour le cas d'une semaine à cheval sur plusieurs mois/années
  $dateI = jll_gen_date(jll_date_annee($dateDebutSemaineCourante),
                        jll_date_mois($dateDebutSemaineCourante),
                        jll_date_jour($dateDebutSemaineCourante),
                        $i);

  $sql = "SELECT *
          FROM categorie, rendezvous
          WHERE rdvIDCategorie=catID
          AND rdvIDUtilisateur=$idAgenda
          AND rdvDate=$dateI
          AND (rdvHeureDebut>=" . $heureMinAffiche*100 . ' OR rdvHeureDebut=-1)' .
        " AND (rdvHeureFin<=" . $heureMaxAffiche*100 . ' OR rdvHeureFin=-1)';
  if(!$owner)
    $sql .= ' AND catPublic=1';

  $sql .= ' ORDER BY rdvDate';

  $R = mysqli_query($GLOBALS['bd'], $sql) or jl_bd_erreur($sql);

  $tab = mysqli_fetch_assoc($R);

  $border = 'border-TRB';
  if($first) {
    $border .= ' border-L';
    $first = false;
  }

  echo '<div class="col-jour ', $border ,'">';
  for($j = 0; $j <= $heureMaxAffiche - $heureMinAffiche; $j++) {
    $dateCourante = jll_gen_date(jll_date_annee($dateDebutSemaineCourante), jll_date_mois($dateDebutSemaineCourante), jll_date_jour($dateDebutSemaineCourante), $i);
    if($owner) {
      $href = 'rendezvous.php?mode=insert' .
            '&hdebut=' . ($heureMinAffiche+$j)*100 .
            '&hfin=' . ($heureMinAffiche+$j+1)*100 .
            '&date=' . $dateCourante;
    } else {
      $href = "#";
    }
    $class = '';
    if($j == $heureMaxAffiche - $heureMinAffiche)
      $class = 'class="case-heure-bas"';
    echo '<a href="', $href ,'" ', $class, '></a>';
  }
  // Ajout des rendez-vous ici - 26+41k  : 1h = 41k

  // $tab est trié (voir la requete SQL)

  while($tab && $tab['rdvDate'] == $dateI) {
    $dateCourante = jll_gen_date(jll_date_annee($dateDebutSemaineCourante), jll_date_mois($dateDebutSemaineCourante), jll_date_jour($dateDebutSemaineCourante), $i);
    if($owner) {
      $href = 'rendezvous.php?mode=update' .
            '&rdvID=' . $tab['rdvID'] .
            '&hdebut=' . $tab['rdvHeureDebut'] .
            '&hfin=' . $tab['rdvHeureFin'] .
            '&categorie=' . $tab['rdvIDCategorie'] .
            '&date=' . $dateCourante .
            '&libelle=' . htmlentities($tab['rdvLibelle']);
    } else {
      $href = '#';
    }
    echo jll_html_rdv($tab['rdvHeureDebut'], $tab['rdvHeureFin'], htmlentities($tab['rdvLibelle']),
                        $tab['catCouleurFond'], $tab['catCouleurBordure'], $href);
    $tab = mysqli_fetch_assoc($R);
  }
  echo '</div>';
  mysqli_free_result($R);
}

mysqli_close($GLOBALS['bd']);

/**
 * Retourne l'année dans une date au format 20150114
 *
 * @param int $date 	Date
 * @return int 			Année de la date
 */
function jll_date_annee($date) {
  return substr($date, 0, 4);
}

/**
 * Retourne le mois dans une date au format 20150114
 *
 * @param int $date 	Date
 * @return int 			Mois de la date
 */
function jll_date_mois($date) {
  return substr($date, 4, 2);
}

/**
 * Retourne le jour dans une date au format 20150114
 *
 * @param int $date 	Date
 * @return int 			Jour de la date
 */
function jll_date_jour($date) {
  return substr($date, 6, 2);
}

/**
 * Genere une date de la forme 20150114 pour le 14 janvier 2015
 *
 * @param int $annee   Année
 * @param int $mois    Numéro du mois (1-12)
 * @param int $jour    Numéro du jour (1-31)
 * @param int $modif   Nombre de jours à ajouter / soustraire à la date passée en paramètre
 */
function jll_gen_date($annee, $mois, $jour, $modif) {
  $nb_jours = date('t', mktime(0, 0, 0, $mois, $jour, $annee));

  if($jour + $modif > $nb_jours) {
    $mois++;
    $modif -= $nb_jours - $jour;
    $jour = $modif;
    if($mois > 12) {
      $annee++;
      $mois = 1;
    }
  } elseif($jour + $modif <= 0) {
    $mois--;
    $modif += $jour;
    if($mois <= 0) {
      $annee--;
      $mois = 12;
    }
    $jour = date('t', mktime(0, 0, 0, $mois, $jour, $annee)) + $modif;
  } else {
    $jour += $modif;
  }

  return $annee*10000 + $mois*100 + $jour;
}

/**
 * Retourne le code HTML permettant d'afficher un bloc de rendez-vous à la bonne hauteur
 *
 * @param int $heureDebut     Heure du début du rendez-vous. Pour 9h30, 930
 * @param int $heureFin       Heure de fin du rendez-vous. Pour 11h15, 1115
 * @param string $libelle     Contenu du rendez-vous
 * @param string $couleurFond Couleur de fond du bloc : sans # (par ex 00FF00)
 * @param string $couleurBordure Couleur de la bordure
 * @param string $href        Lien
 * @param string $couleur     Couleur du texte (optionnel, noir par défaut)
 *
 * @return string             Code HTML du bloc
 */
function jll_html_rdv($heureDebut, $heureFin, $libelle, $couleurFond, $couleurBordure, $href, $couleur = "000000") {
  $res = '<a style="background-color:#'. $couleurFond .';border:solid 2px #'. $couleurBordure .';color:#'. $couleur. ';';

  if($heureDebut == -1) {
    $res .= 'top:17px;height:15px;';
  } else {

    $m = (int) substr($heureDebut, -2);
  	($m == 0) && $m = '';
  	$h = (int) ($heureDebut / 100);
    $m = $m + 60*$h;

    global $heureMinAffiche;
    $debut = 26+(41*($m-$heureMinAffiche*60)/60);

    $res .= 'top:' . $debut . 'px;';

    $m2 = (int) substr($heureFin, -2);
  	($m2 == 0) && $m2 = '';
  	$h2 = (int) ($heureFin / 100);
    $m2 = $m2 + 60*$h2;

    $m = $m2 - $m;
    $fin = (41*$m/60) - 6;

    $res .= 'height:' . $fin . 'px;';
  }
  $res .= '" class="rendezvous" href="' . $href . '">' . $libelle . '</a>';

  return $res;
}

echo '</section>
</section>
<div style="clear:both;"></div>
</section>';

jl_html_pied();

ob_end_flush();
?>
