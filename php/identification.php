<?php

include('bibli_24sur7.php');
$erreur = false;

if(isset($_POST['btnValider'])) {
  jl_bd_connexion();

  $mail = mysqli_real_escape_string($GLOBALS['bd'], $_POST['txtMail']);
  $mdp = mysqli_real_escape_string($GLOBALS['bd'], md5($_POST['txtPasse']));
  $sql = "SELECT utiID, utiNom
          FROM utilisateur
          WHERE utiMail='$mail'
          AND utiPasse='$mdp'";

  $R = mysqli_query($GLOBALS['bd'], $sql) or jl_bd_erreur($sql);

  $D = mysqli_fetch_assoc($R);
  mysqli_free_result($R);
  if($D === NULL) {
    // Erreur : utilisateur non trouvé
    $erreur = true;
  } else {
    session_start();
    $_SESSION['utiID'] = $D['utiID'];
    $_SESSION['utiNom'] = $D['utiNom'];

    // Déconnexion de la base de données
    mysqli_close($GLOBALS['bd']);

    jl_redirige('agenda.php');
    exit();
  }
} else {
	$_POST['txtMail'] = '';
}

jl_html_head('24sur7 | Connexion');

jl_html_bandeau();

echo '<section id="bcContenu">';
echo '<div class="center-page">';

if($erreur) {
  echo '<p><strong>Impossible de vous connecter, veuillez réessayer</strong></p>';
}

echo '<p>Pour vous connecter, veuillez vous identifier.</p>';

echo '<form method="POST">','<table>';

echo jl_form_ligne('Email', jl_form_input(APP_Z_TEXT, 'txtMail', $_POST['txtMail'])),
    jl_form_ligne('Mot de passe', jl_form_input(APP_Z_PASS, 'txtPasse', '')),
    jl_form_ligne(jl_form_input(APP_Z_SUBMIT, 'btnValider', 'S\'identifier'),
      jl_form_input(APP_Z_RESET, 'btnAnnuler', 'Annuler'));

echo '</table>',
    '</form>',
  '<p>Pas encore de compte ? <a href="inscription.php">Inscrivez-vous</a> sans plus tarder !',
  '<p>Vous h&eacute;sitez &agrave; vous inscrire ? Laissez-vous s&eacuteduire par <a href="../html/presentation.html">une pr&eacute;sentation</a> des possibilit&eacute;s de 24sur7',
'</div></section>';

jl_html_pied();

?>
