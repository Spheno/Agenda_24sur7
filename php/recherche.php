<?php
/** @file
 * Permet à l'utilisateur courant de rechercher d'autres utilisateurs
 *
 * @author : Mohamed Lakhal - Emile Jeannin
 */

session_start();
$id = $_SESSION['utiID'];

include 'bibli_24sur7.php';


jl_verifie_session();

jl_bd_connexion();
jl_html_head('24sur7 | Recherche');
jl_html_bandeau(APP_PAGE_RECHERCHE);

/**
* Validation de la saisie et création d'un nouvel utilisateur.
*
* Si la recherche ne donne pas de résultats, un message est renvoyé sous la forme
* d'un tableau. Si il n'y a pas d'erreurs, les résultats sont affichés.
*
* @global array		$_POST		zones de saisie du formulaire
*
* @return array 	Tableau des erreurs détectées
*/
function jll_traitementAffichage() {
	//-----------------------------------------------------
	// Vérification des zones
	//-----------------------------------------------------
	$erreurs = array();

	global $id;

	if ($_POST['txtSearch'] != NULL) {
		if(isset($_POST['btnAbonne']) && is_numeric($_POST['id'])) {
		  if($_POST['btnAbonne'] == 'S\'abonner') {
			// On s'abonne à $_POST['id']

			// On regarde si c'est déjà fait
			$sql = "SELECT * FROM suivi
					WHERE suiIDsuivi = {$_POST['id']}
					AND suiIDsuiveur = $id";
			$R = mysqli_query($GLOBALS['bd'], $sql) or jl_bd_erreur($sql);

			if(! mysqli_fetch_assoc($R)) {
			  // On ajoute alors dans la base
			  $add = "INSERT INTO suivi SET
					  suiIDsuivi = {$_POST['id']},
					  suiIDsuiveur = $id";
			  mysqli_query($GLOBALS['bd'], $add) or jl_bd_erreur($add);
			}
			mysqli_free_result($R);
		  } else {
			// On se désabonne de $_POST['id']
			$del = "DELETE FROM suivi
					WHERE suiIDsuiveur = $id
					AND suiIDsuivi = {$_POST['id']};";
			mysqli_query($GLOBALS['bd'], $del) or jl_bd_erreur($del);
		  }
		}

		$search = mysqli_real_escape_string($GLOBALS['bd'], $_POST['txtSearch']);

		$sqlSearch = "SELECT *
					FROM utilisateur
					WHERE utiNom LIKE '%$search%'
					OR utiMail LIKE '%$search%'";

		$r = mysqli_query($GLOBALS['bd'], $sqlSearch) or jl_bd_erreur($sqlSearch);
		$nb_results = mysqli_num_rows($r);

		// Si le nombre de résultats est supérieur à 0, on continue
		if ($nb_results != 0) {
			echo '<ul id="utiSearch">';
			while ($enr = mysqli_fetch_assoc($r)) {
        $sql = "SELECT *
                FROM suivi
                WHERE suiIDSuivi=$id
                AND suiIDSuiveur={$enr['utiID']}";
        $r2 = mysqli_query($GLOBALS['bd'], $sql) or jl_bd_erreur($sql);

				$aboOrNot = '';
        $abonne = mysqli_fetch_assoc($r2) != NULL ? '[Abonné à votre agenda]' : '';
        mysqli_free_result($r2);

        $sql = "SELECT *
                FROM suivi
                WHERE suiIDSuivi={$enr['utiID']}
                AND suiIDSuiveur=$id";
        $r2 = mysqli_query($GLOBALS['bd'], $sql) or jl_bd_erreur($sql);

        if(mysqli_fetch_assoc($r2) != NULL) {
						$aboOrNot = 'Se désabonner';
				} else {
					$aboOrNot = 'S\'abonner';
				}
        mysqli_free_result($r2);

				echo '<li>',
            '<form style="display:inline;" method="POST">',
						'<input type="hidden" name="id" value="', $enr['utiID'], '">',
            '<input type="hidden" name="txtSearch" value="', $_POST['txtSearch'], '">',
						$enr['utiNom'],
						' - ',
						htmlspecialchars($enr['utiMail']),
						' ',
						$abonne,
						($enr['utiID'] == $id) ? '' : jl_form_input(APP_Z_SUBMIT, 'btnAbonne', $aboOrNot),
            '</form>',
					'</li>';
			}
			mysqli_free_result($r);
			echo '</ul>';
		} else {
			$erreurs[] = 'Pas de résultats';
		}
	} else {
		$erreurs[] = 'Le critère de recherche ne peut être vide';
	}

	// Si il y a des erreurs, la fonction renvoie le tableau d'erreurs
	if (count($erreurs) > 0) {
		return $erreurs;		// RETURN : des erreurs ont été détectées
	}
}

echo '<section id="bcContenu">';
echo '<div class="center-page">';

echo '<form method=POST>',
		'<table>',
			'<tr>',
				'<td>',"Entrez le critère de recherche : ",'</td>',
				'<td>',jl_form_input(APP_Z_TEXT, 'txtSearch', isset($_POST['txtSearch']) ? $_POST['txtSearch'] : ''),'</td>',
				'<td>',jl_form_input(APP_Z_SUBMIT, 'btnSearch', 'Rechercher'),'</td>',
			'</tr>',
		'</table>',
	  '</form>';

if (! isset($_POST['txtSearch'])) {
	$nbErr = 0;
} else {
	// On est dans la phase de soumission du formulaire :
	// => recherche dans la bdd
	// Si aucune erreur n'est détectée, jll_traitementAffichage()
	$erreurs = jll_traitementAffichage();
	$nbErr = count($erreurs);
}

// Si il y a des erreurs on les affiche
if ($nbErr > 0) {
	for ($i = 0; $i < $nbErr; $i++) {
		echo '<p><strong>', $erreurs[$i], '</strong></p>';
	}
}

echo '</div></section>';

jl_html_pied();

mysqli_close($GLOBALS['bd']);
ob_end_flush();
?>
