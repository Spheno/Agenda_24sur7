<?php
/** @file
 * Liste des rendez-vous d'un utilisateur
 *
 * @author : Frederic Dadeau - frederic.dadeau@univ-fcomte.fr
 */

// Bufferisation des sorties
ob_start();

// Inclusion de la bibliothéque
include('bibli_24sur7.php');

// Début de la page
jl_html_head('Rendez-vous utilisateur', '-');

// Connexion à la base de données
jl_bd_connexion();


// Requête de sélection de l'utilisateur 2
$sql = 'SELECT	utiNom, rendezvous.*
		FROM	utilisateur,
				rendezvous
		WHERE utiID = 2
		AND rdvIDUtilisateur = utiID
		ORDER BY rdvDate, rdvHeureDebut';

// Exécution de la requête
$R = mysqli_query($GLOBALS['bd'], $sql) or jl_bd_erreur($sql);

$isFirst = TRUE;

// Affichage du résultat de la requête
while ($D = mysqli_fetch_assoc($R)) {
	if ($D['rdvHeureDebut'] == -1) {
		$heure = 'journ&eacute;e enti&egrave;re';
	} else {
		$heure = jl_heure_claire($D['rdvHeureDebut'])
				.' &agrave; '.jl_heure_claire($D['rdvHeureFin']);
	}

	if ($isFirst) {
		echo '<h2>Utilisateur ', $D['rdvIDUtilisateur'], ' : ',
				htmlentities($D['utiNom'], ENT_QUOTES, 'UTF-8'), '</h2><ul>';
		$isFirst = FALSE;
	}

	echo '<li>', jl_date_claire($D['rdvDate']),
				' - ', $heure,
				' - ', htmlentities($D['rdvLibelle'], ENT_QUOTES, 'UTF-8'), '</li>';
}

// Libère la mémoire associée au résultat $R
mysqli_free_result($R);

// Déconnexion de la base de données
mysqli_close($GLOBALS['bd']);

// fin de la page
echo '</ul></main></body></html>';
?>
