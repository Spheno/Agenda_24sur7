<?php
/** @file
 * Liste des rendez-vous d'un utilisateur
 *
 * @author : Frederic Dadeau - frederic.dadeau@univ-fcomte.fr
 */

// Bufferisation des sorties
ob_start();

// Inclusion de la bibliothéque
include('bibli_24sur7.php');

// Début de la page
ej_html_head('Rendez-vous utilisateur', '-');

// Connexion à la base de données
ej_bd_connexion();

// Requête de sélection de l'utilisateur 2
$sql = 'SELECT	utiNom, rendezvous.*, categorie.*
		FROM	utilisateur,
				rendezvous,
				categorie
		WHERE utiID = 2
		AND catID = rdvIDCategorie
		AND catIDUtilisateur = utiID
		ORDER BY rdvDate, rdvHeureDebut';

// Exécution de la requête
$R = mysqli_query($GLOBALS['bd'], $sql) or ej_bd_erreur($sql);

$isFirst = TRUE;

// Affichage du résultat de la requête
while ($D = mysqli_fetch_assoc($R)) {
	if ($D['rdvHeureDebut'] == -1) {
		$heure = 'journ&eacute;e enti&egrave;re';
	} else {
		$heure = ej_heure_claire($D['rdvHeureDebut'])
				.' &agrave; '.ej_heure_claire($D['rdvHeureFin']);
	}

	$public = ($D['catPublic'] == 0) ? 'font-style: italic;' : '';

	if ($isFirst) {
		echo '<h2>Utilisateur ', $D['rdvIDUtilisateur'], ' : ',
				htmlentities($D['utiNom'], ENT_QUOTES, 'UTF-8'), '</h2><ul>';
		$isFirst = FALSE;
	}

	echo '<li style="', $public, 'background-color: #', htmlentities($D['catCouleurFond'], ENT_QUOTES, 'UTF-8'),
				';border: 1px solid #', htmlentities($D['catCouleurBordure'], ENT_QUOTES, 'UTF-8'),
				';margin: 2px 0">',
				ej_date_claire($D['rdvDate']),
				' - ', $heure,
				' - ', htmlentities($D['rdvLibelle'], ENT_QUOTES, 'UTF-8'), '</li>';
}

// Libère la mémoire associée au résultat $R
mysqli_free_result($R);

// Déconnexion de la base de données
mysqli_close($GLOBALS['bd']);

// fin de la page
echo '</ul></main></body></html>';
?>
