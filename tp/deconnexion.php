<?php
/**
* TP 4 : Déconnexion d'un utilisateur
*
*/

ob_start();
require('bibli_24sur7.php');

// Lancement de la session
session_start();
session_unset();
session_destroy();
// Appel à la fonction de déconnexion et de redirection
jl_exit_session();
?>
