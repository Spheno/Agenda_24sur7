<?php

session_start();

include('bibli_24sur7.php');

jl_verifie_session();

jl_bd_connexion();

jl_html_head('24sur7 | Abonnements');
jl_html_bandeau(APP_PAGE_ABONNEMENTS);

if(isset($_POST['btnAbonne']) && is_numeric($_POST['id'])) {
  if($_POST['btnAbonne'] == 'S\'abonner') {
    // On s'abonne à $_POST['id']
    // On regarde si c'est déjà fait
    $sql = "SELECT * FROM suivi
            WHERE suiIDsuivi={$_POST['id']}
            AND suiIDsuiveur={$_SESSION['utiID']}";
    $R = mysqli_query($GLOBALS['bd'], $sql) or jl_bd_erreur($sql);

    if(!mysqli_fetch_assoc($R)) {
      // On ajoute alors dans la base
      $add = "INSERT INTO suivi SET
              suiIDsuivi={$_POST['id']},
              suiIDsuiveur={$_SESSION['utiID']};";
      mysqli_query($GLOBALS['bd'], $add) or jl_bd_erreur($add);
    }
    mysqli_free_result($R);
  } else {
    // On se désabonne à $_POST['id']
    $del = "DELETE FROM suivi
            WHERE suiIDsuiveur={$_SESSION['utiID']}
            AND suiIDsuivi={$_POST['id']};";
    mysqli_query($GLOBALS['bd'], $del) or jl_bd_erreur($del);
  }
}

echo '<section id="bcContenu">';

echo '<div id="utiFollows">';
echo '<h3>Je suis abonné</h3>';
echo '<ul>';

$sql = "SELECT utiID, utiNom, utiMail
        FROM utilisateur, suivi
        WHERE utiID=suiIDSuivi
        AND suiIDSuiveur={$_SESSION['utiID']}";

$R = mysqli_query($GLOBALS['bd'], $sql) or jl_bd_erreur($sql);

while($res = mysqli_fetch_assoc($R)) {
  echo '<li>',
			$res['utiNom'], ' - ',
			htmlspecialchars($res['utiMail']),
      '<form style="display:inline;" method="POST">',
				'<input type="hidden" name="id" value="', $res['utiID'], '">',
				jl_form_input(APP_Z_SUBMIT, 'btnAbonne', 'Se désabonner'),
      '</form>',
      '</li>';
}
mysqli_free_result($R);

echo '</ul>';
echo '</div>';

echo '<div id="utiFollowings">';
echo '<h3>Ils me suivent</h3>';
echo '<ul>';

$sql = "SELECT utiID, utiNom, utiMail
        FROM utilisateur, suivi
        WHERE utiID=suiIDSuiveur
        AND suiIDSuivi={$_SESSION['utiID']}";

$R = mysqli_query($GLOBALS['bd'], $sql) or jl_bd_erreur($sql);

while($res = mysqli_fetch_assoc($R)) {
  $sql2 = "SELECT *
           FROM suivi
           WHERE suiIDSuiveur={$_SESSION['utiID']}
           AND suiIDSuivi={$res['utiID']}";
  $R2 = mysqli_query($GLOBALS['bd'], $sql2) or jl_bd_erreur($sql2);

  $abo = mysqli_fetch_assoc($R2) != NULL ? 'Se désabonner' : 'S\'abonner';
  echo '<li>',
	    $res['utiNom'], ' - ',
		  htmlspecialchars($res['utiMail']),
      '<form style="display:inline;" method="POST">',
				'<input type="hidden" name="id" value="', $res['utiID'], '">',
				jl_form_input(APP_Z_SUBMIT, 'btnAbonne', $abo),
      '</form>',
    '</li>';
  mysqli_free_result($R2);
}
mysqli_free_result($R);

echo '</ul>';
echo '</div>';
echo '</section>';

jl_html_pied();

mysqli_close($GLOBALS['bd']);
ob_end_flush();
?>
