<?php
// Bufferisation des sorties
ob_start();

// On récupère la session et l'id de l'utilisateur passé par agenda.php
session_start();
$id = $_SESSION['utiID'];

// Inclusion de la bibliothéque
include('bibli_24sur7.php');

// Connexion à la base de données
jl_bd_connexion();

jl_verifie_session();

/**
* Génére le code HTML pour la liste déroulante pour les catégories
*
* @return string Le code HTML de la liste déroulante du choix de catégorie
*/
function jll_form_Categorie($cat){
  global $id;
  $sqlCat = "SELECT *
            FROM categorie
            WHERE catIDUtilisateur = $id";

  $r = mysqli_query($GLOBALS['bd'], $sqlCat) or jl_bd_erreur($r);

  $res = "<select id=categorie name=categorie>";
  while ($enr = mysqli_fetch_assoc($r)) {
	if ($enr['catID'] === $cat) {
		$res .= "<option value=".$enr['catID'].' selected >'.$enr['catNom']."</option>";
	}
    else {
		$res .= "<option value=".$enr['catID'].'>'.$enr['catNom']."</option>";
	}
  }
  $res .= "</select>";
  mysqli_free_result($r);

  return $res;
}

/**
* Génère le code HTML pour l'affichage d'un horaire sous forme de 2 listes déroulantes
* Vérification des horaires saisies
*
* @param string  $name    Nom de la liste déroulante (début ou fin)
* @param integer $heure   Heure récupéré depuis la page agenda.php
* @param integer $minute  Minute récupéré depuis la page agenda.php
*
* @return string Le code HTML des 2 zones de liste
*/
function jll_formHoraire($name, $heure, $minute) {
  $heure=(int)$heure;
  $minute=(int)$minute;

  $res = "<select name='{$name}_h'>";
  for ($i = 0; $i < 24; $i++) {
    if ($i == $heure)
			$res .= "<option value='$i' selected>$i</option>";
		else
			$res .= "<option value='$i'>$i</option>";
  }

  $res .= " </select>:<select name='{$name}_m'>";
  for ($i=0; $i <= 59 ; $i+=15){
    ($i === 0) && $i = "00";
		if ($i == $minute)
			$res .= "<option value='$i' selected>".$i."</option>";
		else
			$res .= "<option value='$i'>".$i."</option>";
	}
	$res .= "</select>";

	return $res;
}

/**
* Validation de la saisie et création d'un nouvel utilisateur.
*
* Les zones reçues du formulaires de saisie sont vérifiées. Si
* des erreurs sont détectées elles sont renvoyées sous la forme
* d'un tableau. Si il n'y a pas d'erreurs, le rendez-vous est ajouté ou mis à jour,
* et une redirection est effectuée.
*
* @global array		$_POST		zones de saisie du formulaire
*
* @return array 	Tableau des erreurs détectées
*/
function jll_traitementAffichage() {
	//-----------------------------------------------------
	// Vérification des zones
	//-----------------------------------------------------
	$erreurs = array();

	global $id;

	// Vérification du nom
	$txtLib = trim($_POST['txtLib']);
	$lib = mb_strlen($txtLib, 'UTF-8');
	if ($lib == '')
	{
		$erreurs[] = 'Le libellé ne doit pas être vide';
	}

	// Vérification de la date et de l'heure
	$jour = trim($_POST['date_j']);
	$mois = trim($_POST['date_m']);
	($mois < 10 ) && $mois = '0'.$mois;
	($jour < 10 ) && $jour = '0'.$jour;
	$annee = trim($_POST['date_a']);
	$dateFormat = $annee.$mois.$jour;

	$heureDebut = trim($_POST['debut_h']);
	$minDebut = trim($_POST['debut_m']);
	$heureFin = trim($_POST['fin_h']);
	$minFin = trim($_POST['fin_m']);
	$debutFormat = $heureDebut.$minDebut;
	$finFormat = $heureFin.$minFin;

	if (! checkdate($mois, $jour, $annee)) {
		$erreurs[] = 'La date n\'est pas valide';
	} else if ($debutFormat >= $finFormat) {
	  $erreurs[] = 'L\'horaire de début doit être strictement inférieure à l\'horaire de fin';
	} else {
		// Vérification que le rendez-vous ne chevauche pas un autre déjà existant
		jl_bd_connexion();

		$ret = mysqli_set_charset($GLOBALS['bd'], "utf8");
    if ($ret == FALSE){
      jl_bd_erreurExit('Erreur lors du chargement du jeu de caractères utf8');
    }

		$date = mysqli_real_escape_string($GLOBALS['bd'], $dateFormat);
		$debut = mysqli_real_escape_string($GLOBALS['bd'], $debutFormat);
		$fin = mysqli_real_escape_string($GLOBALS['bd'], $finFormat);

		$S = "SELECT	count(*)
				FROM	rendezvous
				WHERE	rdvDate = '$date'
				AND rdvHeureDebut <= '$debut'
				AND rdvHeureFin >= '$debut'
				AND rdvHeureDebut <= '$fin'
				AND rdvHeureFin >= '$fin'
				AND rdvIDUtilisateur = '$id'";

	  $R = mysqli_query($GLOBALS['bd'], $S) or jl_bd_erreur($S);

		$D = mysqli_fetch_row($R);

		if ($D[0] > 0 && (!isset($_GET['rdvID']) || !is_numeric($_GET['rdvID']))) {
			$erreurs[] = 'Un rendez-vous est déjà existant sur ce créneau pour cet utilisateur.';
		}
		// Libère la mémoire associée au résultat $R
        mysqli_free_result($R);
	}

	// Si il y a des erreurs, la fonction renvoie le tableau d'erreurs
	if (count($erreurs) > 0) {
		return $erreurs;		// RETURN : des erreurs ont été détectées
	}

	//-----------------------------------------------------
	// Insertion d'un nouveau rendez-vous dans la base de données
	//-----------------------------------------------------
	$lib = mysqli_real_escape_string($GLOBALS['bd'], $txtLib);

	/*$selCat = trim($_POST['categorie']);
	$cat = mysqli_real_escape_string($GLOBALS['bd'], $_POST['categorie']);

	$selCat = "SELECT catID
	      FROM categorie
        WHERE catNom = '$cat'
        AND catIDUtilisateur = '$id'";
  echo "!!!$selCat!!!";

	$rCat = mysqli_query($GLOBALS['bd'], $selCat) or jl_bd_erreur($selCat);
  $enr = mysqli_fetch_assoc($rCat);
  $rdvCatID = $enr['catID'];*/

  if(is_numeric($_POST['categorie'])) {
    $rdvCatID = $_POST['categorie'];
    if(isset($_POST['chkJournee'])) {
      $debut = -1;
      $fin = -1;
    }

    if(isset($_GET['rdvID']) && is_numeric($_GET['rdvID'])) {
      $insertRDV = "UPDATE rendezvous SET
    	    rdvDate = '$date',
    			rdvHeureDebut = '$debut',
    			rdvHeureFin = '$fin',
    			rdvLibelle = '$lib',
    			rdvIDCategorie = '$rdvCatID',
    			rdvIDUtilisateur = '$id'
          WHERE rdvID={$_GET['rdvID']}";
    } else {
      $insertRDV = "INSERT INTO rendezvous SET
    	    rdvDate = '$date',
    			rdvHeureDebut = '$debut',
    			rdvHeureFin = '$fin',
    			rdvLibelle = '$lib',
    			rdvIDCategorie = '$rdvCatID',
    			rdvIDUtilisateur = '$id'";
    }


  	$rInsert = mysqli_query($GLOBALS['bd'], $insertRDV) or jl_bd_erreur($insertRDV);
  }
    // mysqli_free_result($rCat);

  /*
	  // Déconnexion de la base de données
      mysqli_close($GLOBALS['bd']);

	  jl_redirige('agenda.php');
	  exit();			// EXIT : le script est terminé
  */
}

/**
 * Renvoie le code HTML d'un formulaire de rendez-vous selon le mode choisi en paramètre (update ou insert)
 *
 * @param string	$mode		Mode choisi (update ou insert)
 *
 * @return string 	Code HTML généré du formulaire
 */
function jll_formRDV($mode) {
  $title = ($mode == 'update') ? 'Modification' : 'Nouvelle saisie';
  $btnSubmit = ($mode == 'update') ? 'Mettre à jour' : 'Ajouter';
  $btnReset = ($mode == 'update') ? 'Supprimer' : 'Annuler';

  global $date;
  global $hDebut;
  global $hFin;
  global $libelle;

  $cat = isset($_GET['categorie']) ? $_GET['categorie'] : -1;
  $j = substr($date, -2);
  $m = substr($date, 4, 2);
  $a = substr($date, 0, 4);

  if (strlen($hDebut) < 4) {
	$debut_h = substr($hDebut, 0, 1);
  }
  else {
	$debut_h = substr($hDebut, 0, 2);
  }

  if (strlen($hFin) < 4) {
	$fin_h = substr($hFin, 0, 1);
  }
  else {
	$fin_h = substr($hFin, 0, 2);
  }

  $debut_m = substr($hDebut, -2);
  $fin_m = substr($hFin, -2);

  echo "<form method=POST>",
		"<table>",
			"<fieldset>",
			  "<legend>",$title,"</legend>",
				jl_form_ligne('Libellé : ', jl_form_input(APP_Z_TEXT, 'txtLib', $libelle)),
				jl_form_ligne('Date : ', jl_form_date('date',$j, $m, $a)),
				jl_form_ligne('Catégorie : ', jll_form_Categorie($cat)),
				jl_form_ligne('Horaire début : ', jll_formHoraire('debut', $debut_h, $debut_m)),
				jl_form_ligne('Horaire fin : ', jll_formHoraire('fin', $fin_h, $fin_m)),
				jl_form_ligne('Ou ', jl_form_input_check('chkJournee', 1, 'Evénement sur une journée')),
				jl_form_ligne(jl_form_input(APP_Z_SUBMIT, 'btnValider', $btnSubmit),
				jl_form_input(APP_Z_RESET, 'btnAnnuler', $btnReset)),
			"</fiedlset></table></form>";
}

//-----------------------------------------------------
// Détermination de la phase de traitement :
// 1er affichage ou soumission du formulaire
//-----------------------------------------------------
if (! isset($_POST['btnValider'])) {
	// On n'est dans un premier affichage de la page.
	// => On intialise les zones de saisie.
	$nbErr = 0;
	$_POST['txtLib'] = '';
	$_POST['debut_h'] = $_POST['fin_h'] = 6;
	$_POST['debut_m'] = $_POST['fin_m'] = 0;
	$_POST['date_j'] = 01;
	$_POST['date_m'] = 01;
	$_POST['date_a'] = 2000;
} else {
	// On est dans la phase de soumission du formulaire :
	// => vérification des valeurs reçues et création rdv.
	// Si aucune erreur n'est détectée, jll_traitementAffichage()
	$erreurs = jll_traitementAffichage();
	$nbErr = count($erreurs);
}

$mode = isset ($_GET['mode']) ? $_GET['mode'] : 'insert';
$libelle = isset ($_GET['libelle']) ? $_GET['libelle'] : '';
$hDebut = isset ($_GET['hdebut']) ? $_GET['hdebut'] : '';
$hFin = isset ($_GET['hdebut']) ? $_GET['hfin'] : '';
$date = isset ($_GET['date']) ? $_GET['date'] : '';

//-----------------------------------------------------
// Affichage de la page
//-----------------------------------------------------
jl_html_head('24sur7 | Rendez-vous');
jl_html_bandeau(APP_PAGE_NO_SELECTED);
echo '<section id="bcContenu">';

echo '<aside id="bcGauche">';
// Date actuelle
list($JJ, $MM, $AA) = explode('-', date('j-n-Y'));

$jour = isset($_GET['jour']) ? $_GET['jour'] : $JJ;
$mois = isset($_GET['mois']) ? $_GET['mois'] : $MM;
$annee = isset($_GET['annee']) ? $_GET['annee'] : $AA;

if (!checkdate($mois, $jour, $annee)) {
  $jour = $JJ;
  $mois = $MM;
  $annee = $AA;
}

jl_html_calendrier($jour, $mois, $annee);

echo '<section id="categories">';

// Catégories

jl_html_categories();

echo '</section>',
		'</aside>';

echo '<div class="center-page">';

// Si il y a des erreurs on les affiche
if ($nbErr > 0) {
	echo '<ul id="errors"><li><strong>Les erreurs suivantes ont été détectées :</strong></li>';
	for ($i = 0; $i < $nbErr; $i++) {
		echo '<li>', $erreurs[$i], '</li>';
	}
	echo '</ul>';
}

jll_formRDV($mode);

echo '<br><p id="redirect"><a href="agenda.php">Retour à l\'agenda</a></p>';
echo '</div><div style="clear:both;"></div></section>';
jl_html_pied();

ob_end_flush();
?>
