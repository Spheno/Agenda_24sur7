<?php

session_start();

include('bibli_24sur7.php');

jl_verifie_session();

jl_bd_connexion();

jl_html_head('24sur7 | Paramètres');
jl_html_bandeau(APP_PAGE_PARAMETRES);


$sql = "SELECT utiMail
        FROM utilisateur
        WHERE utiID=" . $_SESSION['utiID'];

$R = mysqli_query($GLOBALS['bd'], $sql) or jl_bd_erreur($sql);

$res = mysqli_fetch_assoc($R);
mysqli_free_result($R);

if(isset($_POST['txtNom'])) {
  $nom = mysqli_real_escape_string($GLOBALS['bd'], $_POST['txtNom']);
} else {
  $nom = $_SESSION['utiNom'];
}

if(isset($_POST['txtMail']) && $_POST['txtMail'] != '') {
  $mail = mysqli_real_escape_string($GLOBALS['bd'], $_POST['txtMail']);
} else {
  $mail = '';
}

if(isset($_POST['txtPasse']) && $_POST['txtPasse'] != '' && isset($_POST['txtConfirm']) && $_POST['txtConfirm'] != '' && $_POST['txtPasse'] == $_POST['txtConfirm']) {
  $mdp = md5($_POST['txtPasse']);
} else {
  $mdp = FALSE;
}

if($nom != $_SESSION['utiNom'] || $mail != '' || $mdp != '') {
  $upd = "UPDATE utilisateur SET ";
  $upd .= "utiNom='$nom'";

  if($mail != '') {
    $upd .= ",utiMail='$mail'";
  }
  if($mdp != FALSE) {
    $upd .= ",utiPasse='$mdp'";
  }
  $upd .= "WHERE utiID=" . $_SESSION['utiID'] . ";";

  mysqli_query($GLOBALS['bd'], $upd) or jl_bd_erreur($upd);
}


echo '<section id="bcContenu">';
echo '<div id="paramPage">';
echo '<form method="POST">',
        '<table>',
          '<fieldset>',
            '<legend>Informations sur votre compte</legend>',
  jl_form_ligne('Nom', jl_form_input(APP_Z_TEXT, 'txtNom', $nom, 35)),
  jl_form_ligne('Email', jl_form_input(APP_Z_TEXT, 'txtMail', $mail ? $mail : $res['utiMail'], 35)),
  jl_form_ligne('Mot de passe', jl_form_input(APP_Z_PASS, 'txtPasse', '')),
  jl_form_ligne('Retapez le mot de passe', jl_form_input(APP_Z_PASS, 'txtConfirm', '')),
  jl_form_ligne(jl_form_input(APP_Z_SUBMIT, 'btnValider', 'Mettre à jour'), jl_form_input(APP_Z_RESET, 'btnAnnuler', 'Annuler')),
		'</fieldset>',
    '</table>',
  '</form>';

$jours = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
if(isset($_POST['btnValider']) && isset($_POST['hmin'])) {
  $sommeJours = 0;
  foreach($jours as $jour) {
    $sommeJours += isset($_POST[$jour]) ? $_POST[$jour] : 0;
  }

  $upd = "UPDATE utilisateur SET
          utiJours=$sommeJours";
  if($_POST['hmax'] - $_POST['hmin'] > 0) {
    $upd .= ',utiHeureMin=' . $_POST['hmin'];
    $upd .= ',utiHeureMax=' . $_POST['hmax'];
  }
  $upd .= " WHERE utiID=" . $_SESSION['utiID'] . ';';

  mysqli_query($GLOBALS['bd'], $upd) or jl_bd_erreur($upd);
}

$sql = "SELECT utiJours, utiHeureMin, utiHeureMax
        FROM utilisateur
        WHERE utiID={$_SESSION['utiID']}";
$R = mysqli_query($GLOBALS['bd'], $sql) or jl_bd_erreur($sql);
if($res = mysqli_fetch_assoc($R)) {
  $joursAffiches = $res['utiJours'];
  $heureMinAffiche = $res['utiHeureMin'];
  $heureMaxAffiche = $res['utiHeureMax'];
} else {
  $joursAffiches = 127;
  $heureMinAffiche = 8;
  $heureMaxAffiche = 20;
}
mysqli_free_result($R);

echo '<br><form id="chkJours" method="POST">',
      '<table>',
        '<fieldset>',
        '<legend>Options d\'affichage du calendrier',
  jl_form_ligne('Jours affichés', jl_form_input_check('lundi', 64, 'Lundi', $joursAffiches >= pow(2, 6) && substr(decbin($joursAffiches), -7, 1) === '1') .
                                  jl_form_input_check('mardi', 32, 'Mardi', $joursAffiches >= pow(2, 5) && substr(decbin($joursAffiches), -6, 1) === '1') .
                                  jl_form_input_check('mercredi', 16, 'Mercredi', $joursAffiches >= pow(2, 4) && substr(decbin($joursAffiches), -5, 1) === '1') . '<br>' .
                                  jl_form_input_check('jeudi', 8, 'Jeudi', $joursAffiches >= pow(2, 3) && substr(decbin($joursAffiches), -4, 1) === '1') .
                                  jl_form_input_check('vendredi', 4, 'Vendredi', $joursAffiches >= pow(2, 2) && substr(decbin($joursAffiches), -3, 1) === '1') .
                                  jl_form_input_check('samedi', 2, 'Samedi', $joursAffiches >= pow(2, 1) && substr(decbin($joursAffiches), -2, 1) === '1') . '<br>' .
                                  jl_form_input_check('dimanche', 1, 'Dimanche', $joursAffiches >= pow(2, 0) && substr(decbin($joursAffiches), -1, 1) === '1')),
  jl_form_ligne('Heure minimale', jll_heure('hmin', $heureMinAffiche)),
  jl_form_ligne('Heure maximale', jll_heure('hmax', $heureMaxAffiche)),
  jl_form_ligne(jl_form_input(APP_Z_SUBMIT, 'btnValider', 'Mettre à jour'), jl_form_input(APP_Z_RESET, 'btnAnnuler', 'Annuler')),
		'</fieldset>',
      '</table>',
    '</form>';

echo '<br>',
        '<fieldset>',
          '<legend>Vos catégories</legend>';

$sql = "SELECT *
        FROM categorie
        WHERE catIDUtilisateur={$_SESSION['utiID']}";
$R = mysqli_query($GLOBALS['bd'], $sql) or jl_bd_erreur($sql);

$nbCategories = mysqli_affected_rows($GLOBALS['bd']);

mysqli_free_result($R);
// On refera la requete afin que l'utilisateur puisse voir les changements sans recharger la page

/*
 Récupération des données en POST
*/
if(isset($_POST['action'])) {
  if(substr($_POST['action'], 0, 6) == 'update') {
    // Simple mise à jour de la catégorie
    $id = substr($_POST['action'], 6);
    $catNom = $_POST['txtCatNom'];
    $catFond = $_POST['txtCatFond'];
    $catBordure = $_POST['txtCatBordure'];
    $public = isset($_POST['public']) ? 1 : 0;

    if($catNom != '' && $catFond != '' && $catBordure != '' && ctype_alnum($catFond) && ctype_alnum($catBordure)) {
      $catNom = mysqli_real_escape_string($GLOBALS['bd'], $catNom);
      $upd = "UPDATE categorie
              SET
                catNom='$catNom',
                catCouleurFond='$catFond',
                catCouleurBordure='$catBordure',
                catPublic=$public
              WHERE
                  catID=$id
              AND catIDUtilisateur={$_SESSION['utiID']};";
      mysqli_query($GLOBALS['bd'], $upd) or jl_bd_erreur($upd);
    }

  } elseif($nbCategories > 1) {
    // Suppression de la catégorie
    if(substr($_POST['action'], 0, 6) == 'delete') {
      // On affiche une confirmation
      $id = substr($_POST['action'], 6);
      echo '<div class="warning">',
            'Supprimer la catégorie et les rendez-vous et événements associés ?',
            '<form method="POST">',
              '<input type="hidden" name="catID" value="', $id, '">',
              '<input type="submit" id="btnValider" name="action" value="Confirmer">',
            '<form>',
          '</div>';
    } elseif($_POST['action'] == 'Confirmer') {
      // On supprime réellement car la confirmation a eu
      $id = $_POST['catID'];

      // Suppression des rendez-vous
      if(is_numeric($id)) { // Si ce n'est pas un nombre, c'est qu'on l'a modifié intentionnellement dans le code HTML
        $dlt = "DELETE FROM rendezvous
                WHERE rdvIDCategorie=$id";

        mysqli_query($GLOBALS['bd'], $dlt) or jl_bd_erreur($dlt);

        // Suppression de la categorie
        $dlt = "DELETE FROM categorie
                WHERE catID=$id";

        mysqli_query($GLOBALS['bd'], $dlt) or jl_bd_erreur($dlt);
      }
    }
  } else {
    echo '<div class="warning">',
          'Vous ne pouvez pas suppimer votre dernière catégorie',
        '</div>';
  }
}

if(isset($_POST['txtAjoutNom']) && $_POST['txtAjoutNom'] != '') {
  $nom = mysqli_real_escape_string($GLOBALS['bd'], $_POST['txtAjoutNom']);
  if(ctype_alnum($_POST['txtAjoutFond']) && ctype_alnum($_POST['txtAjoutBordure'])) {
    $publicAjout = isset($_POST['publicAjout']) ? 1 : 0;

    $ajt = "INSERT INTO categorie SET
    			  catNom='$nom',
    			  catCouleurFond='{$_POST['txtAjoutFond']}',
    			  catCouleurBordure='{$_POST['txtAjoutBordure']}',
    			  catIDUtilisateur='{$_SESSION['utiID']}',
    			  catPublic=$publicAjout";
    mysqli_query($GLOBALS['bd'], $ajt) or jl_bd_erreur($ajt);
  }
}

$R = mysqli_query($GLOBALS['bd'], $sql) or jl_bd_erreur($sql);

while($res = mysqli_fetch_assoc($R)) {
  jll_html_categorie($res['catID'], $res['catNom'], $res['catCouleurFond'], $res['catCouleurBordure'], $res['catPublic'] == 1);
}


echo '<form method="POST">',
	  '<legend id="newCat">Nouvelle catégorie :</legend>',
      '<label for="txtAjoutNom"> Nom : ',
        jl_form_input(APP_Z_TEXT, 'txtAjoutNom', '', 10),
	  '</label>',
      '<label for="txtAjoutFond"> Fond : ',
        jl_form_input(APP_Z_TEXT, 'txtAjoutFond', '', 5),
	  '</label>',
      '<label for="txtAjoutBordure"> Bordure : ',
        jl_form_input(APP_Z_TEXT, 'txtAjoutBordure', '', 5),
	  '</label>',
      jl_form_input_check('publicAjout', 1, 'Public '),
      jl_form_input(APP_Z_SUBMIT, 'btnValider', 'Ajouter'),
     '</form><br>';

echo '</fieldset>';
echo '</div></section>';
jl_html_pied();

mysqli_close($GLOBALS['bd']);

/**
 * Génère le code HTML (affiche) d'un ligne de catégorie
 *
 * @param int $id ID de la catégorie
 * @param string $nom Nom de la catégorie
 * @param string $couleurFond Couleur de fond (par exemple FFDD00)
 * @param string $couleurBordure Couleur de bordure (par exemple FFDD00)
 * @param boolean $public Si la catégorie est publique
 */
function jll_html_categorie($id, $nom, $couleurFond, $couleurBordure, $public) {
  echo '<form method="POST">',
        '<label for="txtCatNom"> Nom : ',
          jl_form_input(APP_Z_TEXT, 'txtCatNom', $nom, 10),
		'</label>',
        '<label for="txtCatFond"> Fond : ',
          jl_form_input(APP_Z_TEXT, 'txtCatFond', $couleurFond, 5),
		'</label>',
        '<label for="txtCatBordure"> Bordure : ',
          jl_form_input(APP_Z_TEXT, 'txtCatBordure', $couleurBordure, 5),
		'</label>',
        jl_form_input_check('public', 1, 'Public ', $public),
        '<span style="background-color:#', $couleurFond, ';',
                     'border:2px solid #', $couleurBordure, ';',
                     'color:#000;',
					 'padding: 1px 1em;',
                     '">Aperçu</span>',
        ' <input type="image" src="../images/save.png" alt="submit" name="action" value="update', $id, '">',
        ' <input type="image" src="../images/delete.png" alt="submit" name="action" value="delete', $id, '">',
       '</form><br>';
}

/**
 * Génère le code HTML d'une liste afin de sélectionner une heure
 *
 * @param string  $name   Nom de la liste
 * @param integer $heure  Heure à afficher par défaut
 * @return string         Code HTML
 */
function jll_heure($name, $heure) {
  $res = "<select name='{$name}'>";
  for ($i = 0; $i < 24; $i++) {
    if ($i == $heure)
			$res .= "<option value='$i' selected>$i:00</option>";
		else
			$res .= "<option value='$i'>$i:00</option>";
  }
  return $res;
}

?>